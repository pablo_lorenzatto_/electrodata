import datetime
import time
import requests
from scrapy.http import HtmlResponse
import config

areas = [
    ("Edenor", "http://www.enre.gov.ar/web/web.nsf/Inicio_Edenor?openform"),
    ("Edesur", "http://www.enre.gov.ar/web/web.nsf/Inicio_Edesur?openform")
]
tipos = ["CortesPreventivos", "InterrupcionesServicio", "CortesMantenimiento"]

data = []
data_usuarios = []
curdatetime = (datetime.datetime.now() +
               datetime.timedelta(hours=2)).strftime("%d/%m/%Y %H:%M")
for (narea, url) in areas:
    body = requests.get(url, timeout=30).content
    response = HtmlResponse(body=body, url=url)
    hora = "-"
    rh = [t for t in response.css(
        "span.nota::text").extract() if "Hora de" in t]
    try:
        if len(rh) == 1:
            hora = rh[0].split(": ")[1]
    except:
        print "No se pudo extraer la hora: %s" % rh
    for tipo in tipos:
        for s in response.css("#%s > tr" % tipo)[1:]:
            cells = s.css("td::text").extract()
            if len(cells) == 5:
                data += [[
                    curdatetime,
                    narea,
                    hora,
                    tipo,
                    cells[0],
                    cells[1],
                    cells[2],
                    cells[4],
                    cells[3],
                ]]

    try:
        cells = [curdatetime]
        cells += [narea]
        cells += [s.strip() for s in response.css(
            "table#Informacion_Superior").xpath(".//td//text()").extract()[2:]]
        cells += [response.css("table#Informacion_pie").xpath(
            ".//td//text()").extract()[1].strip()]
        if len(cells) == 5:
            data_usuarios += [cells]
        else:
            cells = [curdatetime, narea, "-", "-", "-"]
    except:
        cells = [curdatetime, narea, "-", "-", "-"]
        print "No se pudo extraer el conteo de usuarios afectados: %s" % cells

outf = open(config.files["afectados_detalle"], "a")
newlines = '\n'.join([','.join(['"%s"' % c for c in r]) for r in data])
newlines = newlines.encode('utf-8').strip() + "\n"
print newlines
outf.write(newlines)
outf.close()

outf = open(config.files["afectados_daily"], "a")
newlines = '\n'.join([','.join(['"%s"' % c for c in r])
                      for r in data_usuarios])
newlines = newlines.encode('utf-8').strip() + "\n"
print newlines
outf.write(newlines)
outf.close()
