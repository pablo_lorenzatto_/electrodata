# Datos generados
Este repositorio contiene los scripts necesarios para generar los siguientes datos:

* Usuarios afectados por los cortes de luz desagregado:
    *  Tabla accesible a traves de esta url:
        *  http://datosvarios.stagings.com.ar/cortes_enre_scrap.csv
    - Campos
        - AREA DE CONCESION
        - HORA DE ACTUALIZACION
        - TIPO DE CORTE
        - PARTIDO
        - LOCALIDAD / BARRIO
        - SUBESTACION / ALIMENTADOR
        - HORA ESTIMADA DE NORMALIZACION
        - CANTIDAD DE USUARIOS AFECTADOS
    - Urls de donde se extraen los datos:
        - http://www.enre.gov.ar/web/web.nsf/Inicio_Edenor
        - http://www.enre.gov.ar/web/web.nsf/Inicio_Edesur
    - Implementado en:
        - cortes_enre_scraper.py

- Cantidad de usuarios afectados por el corte de luz actual
    -  Tabla accesible a traves de esta url:
        - http://datosvarios.stagings.com.ar/cortes_enre_usuarios_scrap.csv
    - Campos
        - Hora de actualizacion
        - Empresa
        - Usuarios sin conexion en este momento
        - Usuarios con conexion
        - Usuarios sin conexion el dia de ayer
    - Urls de donde se extraen los datos:
        - http://www.enre.gov.ar/web/web.nsf/Inicio_Edenor
        - http://www.enre.gov.ar/web/web.nsf/Inicio_Edesur
    - Implementado en:
        - cortes_enre_scraper.py

- Cantidad de usuarios afectados por el corte de luz actual
    -  Tabla accesible a traves de esta url:
        - https://bitbucket.org/pablo_lorenzatto_/electrodata/raw/master/cam_scrap.csv
        - http://datosvarios.stagings.com.ar/cam_scrap.csv
    - Campos
        - Hora de actualizacion
        - Demanda en MW
    - Urls de donde se extraen los datos:
        - http://portalweb.cammesa.com/default.aspx
    - Implementado en:
        - camesa_scraper.py

# Pusheo de datos
Actualmente los datos generados se estan guardando en dos lugares mediante el script update_script.sh:

* Git privado de bitbucket: pablo_lorenzatto_/electrodata
* Directorio /var/www/datosvarios/ en 104.131.29.59(datosvarios.stagings.com.ar) mediante scp (si el tamaño del archivo se hace muy grande convendria usar rsync en vez)

En ambos casos se requiere que la key del usuario que esta cargando los datos este en el servidor destino, sea bitbucket o datosvarios.

<!-- # Inicio de scrips
Los scripts se estan corriendo en una shell desatachada de screen (esto es por comodidad no por que sea lo mejor o mas prolijo):

screen -L -S cortes_enre_scraper bash -c "while true; do python /home/gcbadaemons/cortes_enre_scraper.py; sleep 2; done"  
screen -L -S cammesa_updater watch -n 120 bash /home/gcbadaemons/update_script.sh -->

# Instalacion de Dependencias
$ pip install requests==2.2.1 scrapy==1.0.3 # Otras versiones tambien podrian funcionar

# Configuracion
Los path de los archivos de output estan en config.py
Cambiar home directory en la primer linea de update_script.sh

Crons:

 - Cada 3 minutos deber correr camesa_scraper.py

*/3 * * * * /home/gcbadaemons/electrodata/camesa_scraper.py

- Cada 55 minutos debe correr cortes_enre_scraper.py

*/55 * * * * /home/gcbadaemons/electrodata/cortes_enre_scraper.py

- Cada 3 minutos debe correr el updater update_script.sh que sincroniza los datos

*/3 * * * * /home/gcbadaemons/electrodata/update_script.sh

