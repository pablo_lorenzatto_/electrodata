import datetime
import time
import requests
from scrapy.http import HtmlResponse
import config

url = "http://portalweb.cammesa.com/default.aspx"

body = requests.get(url, timeout=30).content
response = HtmlResponse(body=body, url=url)
power = response.xpath(
    ".//td[contains(text(),'Demanda SADI')]/../td[2]/text()").extract()[0]
dt = response.css(".cssFunciones").xpath("./text()").extract()[0]
outf = open(config.files["demanda"], "a")
#line = "%s,%s,%s\n" % (datetime.datetime.now(), dt, power)
line = "%s,%s\n" % (dt, power)
print line
outf.write(line)
outf.close()
